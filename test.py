import sys

def hello(name):
    print 'Hello {}'.format(name)

if __name__ == "__main__":
    name = sys.argv[1]
    hello(name)
